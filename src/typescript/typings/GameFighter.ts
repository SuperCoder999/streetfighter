import { FighterDetails } from "./entities";

export default interface GameFighter extends FighterDetails {
	isInBlock: boolean;
	currentHealth: number;
	lastCriticalHitMs: number;
}

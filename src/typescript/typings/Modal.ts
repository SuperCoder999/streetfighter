export default interface Modal {
	title: string;
	bodyElement: HTMLElement;
	onClose?: () => void;
}

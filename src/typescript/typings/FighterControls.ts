export default interface FighterControls {
	attackKeyCode: string;
	defendKeyCode: string;
	criticalHitCombination: string[];
}

export interface FighterControlsSet {
	first: FighterControls;
	second: FighterControls;
}

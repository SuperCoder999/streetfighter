import { CONTROLS, CRITICAL_HIT_DELAY_MS } from "./constants/controls";
import { fighterDetailsToGameFighter } from "./helpers/fighterHelper";
import { chance } from "./helpers/randomHelper";
import { FighterDetails } from "./typings/entities";
import GameFighter from "./typings/GameFighter";

interface HealthSetter {
	(value: number): void;
}

export function fight(
	firstFighter: FighterDetails,
	secondFighter: FighterDetails,
	setFirstHealth: HealthSetter,
	setSecondHealth: HealthSetter
): Promise<FighterDetails> {
	const firstState: GameFighter = fighterDetailsToGameFighter(firstFighter);
	const secondState: GameFighter = fighterDetailsToGameFighter(secondFighter);
	const pressedKeys = new Set<string>();

	return new Promise<FighterDetails>((resolve) => {
		const getOther = (fighter: GameFighter): GameFighter => {
			if (fighter._id === firstState._id) {
				return secondState;
			}

			return firstState;
		};

		const setHealth = (fighter: GameFighter): void => {
			const health = Math.floor(fighter.currentHealth);

			if (fighter._id === firstFighter._id) {
				return setFirstHealth(health);
			}

			return setSecondHealth(health);
		};

		const checkDefendKey = (key: string): GameFighter | null => {
			if (key === CONTROLS.first.defendKeyCode) {
				return firstState;
			} else if (key === CONTROLS.second.defendKeyCode) {
				return secondState;
			}

			return null;
		};

		const checkAttackKey = (key: string): GameFighter | null => {
			if (key === CONTROLS.first.attackKeyCode) {
				return firstState;
			} else if (key === CONTROLS.second.attackKeyCode) {
				return secondState;
			}

			return null;
		};

		const checkAllPressed = (keys: string[]): boolean => {
			return keys.every((key) => pressedKeys.has(key));
		};

		const checkCriticalHitKeys = (): GameFighter | null => {
			if (checkAllPressed(CONTROLS.first.criticalHitCombination)) {
				return firstState;
			} else if (checkAllPressed(CONTROLS.second.criticalHitCombination)) {
				return secondState;
			}

			return null;
		};

		const defend = (fighter: GameFighter): void => {
			fighter.isInBlock = true;
		};

		const notDefend = (fighter: GameFighter): void => {
			fighter.isInBlock = false;
		};

		const checkWin = (attacker: GameFighter, enemy: GameFighter): void => {
			if (enemy.currentHealth <= 0) {
				document.removeEventListener("keydown", onPress);
				document.removeEventListener("keyup", onRelease);

				resolve(attacker);
			}
		};

		const attack = (attacker: GameFighter): void => {
			const enemy = getOther(attacker);

			if (enemy.isInBlock) {
				return;
			}

			const damage = getDamage(attacker, enemy);
			enemy.currentHealth -= damage;

			setHealth(enemy);
			checkWin(attacker, enemy);
		};

		const criticalHit = (attacker: GameFighter): void => {
			const sinceLastCriticalHit =
				performance.now() - attacker.lastCriticalHitMs;

			if (sinceLastCriticalHit < CRITICAL_HIT_DELAY_MS) {
				return;
			}

			const enemy = getOther(attacker);
			const damage = getCriticalHitPower(attacker);

			enemy.currentHealth -= damage;
			attacker.lastCriticalHitMs = performance.now();

			setHealth(enemy);
			checkWin(attacker, enemy);
		};

		const onPress = (event: KeyboardEvent): void => {
			pressedKeys.add(event.code);
			const maybeDefender = checkDefendKey(event.code);

			if (maybeDefender) {
				defend(maybeDefender);
			}

			const maybeAttacker = checkAttackKey(event.code);

			if (maybeAttacker) {
				attack(maybeAttacker);
			}

			const maybeCriticalAttacker = checkCriticalHitKeys();

			if (maybeCriticalAttacker) {
				criticalHit(maybeCriticalAttacker);
			}
		};

		const onRelease = (event: KeyboardEvent): void => {
			pressedKeys.delete(event.code);
			const maybeDefender = checkDefendKey(event.code);

			if (maybeDefender) {
				notDefend(maybeDefender);
			}
		};

		document.addEventListener("keydown", onPress);
		document.addEventListener("keyup", onRelease);
	});
}

export function getDamage(attacker: GameFighter, enemy: GameFighter): number {
	return Math.max(getHitPower(attacker) - getBlockPower(enemy), 0);
}

export function getCriticalHitPower(attacker: GameFighter): number {
	return attacker.attack * 2;
}

export function getHitPower(fighter: GameFighter): number {
	return fighter.attack * chance();
}

export function getBlockPower(fighter: GameFighter): number {
	return fighter.defense * chance();
}

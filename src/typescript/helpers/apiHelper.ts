import HttpMethod from "../typings/HttpMethod";
import { fightersDetails, fighters } from "./mockData";
import { API_URL, USE_MOCK_API } from "../constants/api";
import { decodeBase64AsObject } from "./base64Helper";
import HttpError from "../typings/HttpError";
import { ApiEntity, FighterDetails } from "../typings/entities";

export default async function callApi<T extends ApiEntity>(
	endpoint: string,
	method: HttpMethod
): Promise<T> {
	const url: string = API_URL + endpoint;

	const options: RequestInit = {
		method,
	};

	return USE_MOCK_API ? fakeCallApi<T>(endpoint) : realCallApi<T>(url, options);
}

async function realCallApi<T extends ApiEntity>(
	url: string,
	options: RequestInit
): Promise<T> {
	const response: T = await fetch(url, options)
		.then((response) =>
			response.ok
				? response.json()
				: Promise.reject(new HttpError("Failed to load", response.status))
		)
		.then((result) => decodeBase64AsObject<T>(result))
		.catch((error) => {
			throw error;
		});

	return response;
}

async function fakeCallApi<T extends ApiEntity>(endpoint: string): Promise<T> {
	const response =
		endpoint === "fighters.json" ? fighters : getFighterDetailsById(endpoint);

	const promiseFunc = (
		resolve: (value: T) => void,
		reject: (error: Error) => void
	): void => {
		if (response) {
			return resolve(response as T);
		}

		return reject(new HttpError("Failed to load", 404));
	};

	return new Promise((resolve, reject) => {
		setTimeout(() => promiseFunc(resolve, reject), 500);
	});
}

function getFighterDetailsById(endpoint: string): FighterDetails | undefined {
	const start: number = endpoint.lastIndexOf("/");
	const end: number = endpoint.lastIndexOf(".json");
	const id: string = endpoint.substring(start + 1, end);

	return fightersDetails.find((f) => f._id === id);
}

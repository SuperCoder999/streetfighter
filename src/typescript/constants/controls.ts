import millis from "../helpers/millisecondsHelper";
import { FighterControlsSet } from "../typings/FighterControls";

export const CRITICAL_HIT_DELAY_MS = millis(10);

export const CONTROLS: FighterControlsSet = {
	first: {
		attackKeyCode: "KeyA",
		defendKeyCode: "KeyD",
		criticalHitCombination: ["KeyQ", "KeyW", "KeyE"],
	},
	second: {
		attackKeyCode: "KeyJ",
		defendKeyCode: "KeyL",
		criticalHitCombination: ["KeyU", "KeyI", "KeyO"],
	},
};

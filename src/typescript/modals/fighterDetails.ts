import { createElement } from "../helpers/domHelper";
import { FighterDetails } from "../typings/entities";
import { showModal } from "./modal";

export function showFighterDetailsModal(fighter: FighterDetails): void {
	const title = "Fighter info";
	const bodyElement = createFighterDetails(fighter);
	showModal({ title, bodyElement });
}

function createFighterDetails(fighter: FighterDetails): HTMLElement {
	const { name, attack, defense, health, source } = fighter;

	const fighterDetails = createElement({
		tagName: "div",
		className: "modal-body",
	});

	const imageElement = createSmallFighterImage(source);
	const nameElement = createTextDiv(name, "fighter-name");
	const attackElement = createTextDiv(`Attack ${attack}`);
	const defenseElement = createTextDiv(`Defense: ${defense}`);
	const healthElement = createTextDiv(`Health: ${health}`);

	fighterDetails.append(
		nameElement,
		imageElement,
		attackElement,
		defenseElement,
		healthElement
	);

	return fighterDetails;
}

export function createSmallFighterImage(source: string): HTMLElement {
	const attributes = { src: source };

	const element = createElement({
		tagName: "img",
		className: "fighter-image",
		attributes,
	});

	return element;
}

function createTextDiv(text: string, className?: string): HTMLElement {
	const element = createElement({ tagName: "div", className });
	element.innerText = text;

	return element;
}

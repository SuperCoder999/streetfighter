import { createElement } from "../helpers/domHelper";
import { Fighter } from "../typings/entities";
import { createSmallFighterImage } from "./fighterDetails";
import { showModal } from "./modal";

export function showWinnerModal(fighter: Fighter): void {
	const winnerElement = createElement({
		tagName: "div",
		className: "modal-body",
	});

	const nameElement = createElement({
		tagName: "div",
		className: "fighter-name",
	});

	nameElement.innerHTML = `The winner is ${fighter.name}`;

	const imageElement = createSmallFighterImage(fighter.source);

	winnerElement.append(nameElement, imageElement);

	showModal({
		title: "Game finished!",
		bodyElement: winnerElement,
		onClose: () => window.location.reload(),
	});
}
